load image_data

% get masks and normalize the images by their medians for display
SDR_SE_mask = abs(SDR_SE_data) > 0.25*max(abs(SDR_SE_data(:)));
SDR_SE_norm = SDR_SE_data./median(abs(SDR_SE_data(SDR_SE_mask)));

maran_SE_mask = abs(maran_SE_data) > 0.4*max(abs(maran_SE_data(:)));
maran_SE_norm = maran_SE_data./median(abs(maran_SE_data(maran_SE_mask)));

SDR_GRE_mask = abs(SDR_GRE_data) > 0.3*max(abs(SDR_GRE_data(:)));
SDR_GRE_norm = SDR_GRE_data./median(abs(SDR_GRE_data(SDR_GRE_mask)));

maran_GRE_mask = abs(maran_GRE_data) > 0.5*max(abs(maran_SE_data(:)));
maran_GRE_norm = maran_GRE_data./median(abs(maran_SE_data(maran_GRE_mask)));

figure;
im([maran_SE_norm SDR_SE_norm].');

figure;
im([maran_GRE_norm SDR_GRE_norm].');
