#!/usr/bin/env python
__author__ = "Chris Hasselwander"
__copyright__ = "Copyright 2016"
__credits__ = ["Chris Hasselwander", "Will Grissom"]
__version__ = "1.0.1"
__status__ = "Production"

from pylab import *
import pulse_shape
import matplotlib.pyplot as plt

import time
import datetime
import threading
import thread
import multiprocessing
import pickle
from tabulate import tabulate
import scipy.signal as sig
from scipy import interpolate
from test import *

execfile("spinecho_flowgraph.py")	# run flow graph

# data structure for output data
class dat(object):
	def __init__(self):
		self.rawdata = ()		# Initialize rawdata
		self.kdata = ()			# Initialize kdata
		self.imdata = ()		# Initialize imdata
	
	def recon(self):
	# 2d fft to create image data
		self.kdata = np.zeros([params.NPE,params.readout_length],complex)	# initialize matrix
		foo = np.reshape(self.rawdata,[params.NPE*params.nav,params.readout_length]) # format rawdata
		
		for ii in range(params.NPE):
			# Average data
			self.kdata[ii,:] = np.sum(foo[ii*params.nav:ii*params.nav+params.nav,:],axis=0)
		
		# Decimate data to requested matrix size
		if params.readout_length%params.SI!=0:
			f2 = ceil(params.readout_length/params.SI)*params.SI	# find common multiple
			x = np.arange(0,params.readout_length)					# set new data indicies
			f = interpolate.interp1d(x,data.kdata,axis=1)			# interpolate to new indicies
			# new x vector
			xnew = np.arange(0,params.readout_length-1,(params.readout_length-1)/f2)
			self.kdata = f(xnew)
		
		self.kdata = sig.decimate(self.kdata,int(self.kdata.shape[1]/params.SI),60,'fir',axis=1)		# Decimate to matrix size with 60 tap FIR filter
		self.imdata = np.fft.fftshift(np.fft.ifft2(np.fft.fftshift(self.kdata)))	# ifft recon
		
		# Fix RF chopping if present
		if (params.ischopped==1):
			self.imdata = np.roll(self.imdata,int(params.NPE/2),axis=0)
			self.imdata[0,:] = self.imdata[1,:]
		
		plt.figure()
		plt.imshow(np.abs(self.imdata),cmap=cm.gray,interpolation='none');draw()	# display image

class pulse(object):
	# Pulse class holds pulse data and when it starts
	def __init__(self,data,start):
		self.data = data;
		self.start = start;
		
class bar:

	def __init__(self):
		return 0;
		
pulses = bar	# class to hold pulse objects
	
class pulse_params(object):		   # scan parameters
	
	def __init__(self):
		# set some defaults
		self.samp_rate = tb.samp_rate
		
		global x
		x=0
		global y
		y=1
		global z
		z=2
		
		# Pull Parameters from spinecho_config.txt
		f = open('spinecho_config.txt', 'r')
		foo = {}
		for line in f:
			k,v,bar = [q.strip() for q in line.split(',')]
			if k == 'param_file':
				self.filename = v					# special treatment of 'param_file'
			elif k == 'interactive_mode':
				global interactive_mode				# setting interactive mode
				interactive_mode = float(v)
			elif k == 'leader_ID':
				self.leader_ID = "serial = " + v	# setting leader radio serial id
			elif k == 'follower_ID':
				self.follower_ID = "serial = " + v	# setting follower radio serial id
			else:   
				foo[k] = float(v)					# build dictionary of param names

		f.close()
		
		self.fudge = 20e-6
		self.__dict__.update(foo)					# add params from file to pulse_param dict
		
		self.NPE = int(self.NPE)

		# load calibration information from cal.pkl
		try:
			info = open('cal.pkl', 'rb')
			calinfo = pickle.load(info)
			info.close()
			
			self.offset = (calinfo["offset"])
			self.power_auc = (calinfo["auc"])
			print "UPDATED CENTER FREQUENCY OFFSET AND POWER AUC FROM CALIBRATION FILE"
		except:
			print "power calibration has not been run"
			
		# load gradient calibration information from Gcal.pkl
		try:
			info = open('Gcal.pkl', 'rb')
			calinfo = pickle.load(info)
			info.close()
			
			self.Grad_str = calinfo["Grad_str"]
			print "UPDATED GRADIENT STRENGTHS FROM CALIBRATION FILE"
		except:
			print "Gradient calibration has not been run"
		
		# set other parameters
		self.readdir = int(self.readdir)
		self.phasedir = int(self.phasedir)
		self.slicedir = int(self.slicedir)
				
		self.phase_namp = self.NPE
		
		self.nav = int(self.nav)
		self.readout_length = round(float(self.samp_rate)/self.BW*self.SI)
		self.readout_time = self.readout_length/self.samp_rate
		self.BW = self.samp_rate*float(self.SI)/self.readout_length
		length = int(self.samp_rate*(self.fudge+self.p90/2+self.TE+self.readout_time/2))
		self.xgrad = ravel(zeros([length,1],float))
		self.ygrad = ravel(zeros([length,1],float))
		self.zgrad = ravel(zeros([length,1],float))
		
		self.dead = 50e-6
		self.rephase_fudge = 1.
		self.prephasor_fudge = 1.
		self.underflow = 0
		self.read_fudge = 1
		
		

	def p90_sinc_pulse(self):
		### Excitation Pulse
		amp = self.power_auc*self.samp_rate
		data = pulse_shape.wsinc(self.p90,self.TBW,self.slice_shift,self.samp_rate)*amp

		# check if aplitude is greater than 1
		if ((np.max(np.real(data))>1) or (np.max(np.imag(data))>1)):
			warnings.simplefilter('error', UserWarning)
			warnings.warn('EXCITATION PULSE AMPLITUDE GREATER THAN 1V')

		tb.ex_pulse.set_data(data)				  # update pulse in flowgraph
		start = int(params.gr_ramp)		# define pulse start time
		pulses.ex = pulse(data,start)				# create pulse object
		tb.set_ex_delay(start) 						# update pulse delay
		if (self.ischopped == 1):
			tb.ex_pulse.set_amps(1,-2,2,self.nav)	# set up RF chopping
		else:
			tb.ex_pulse.set_amps(1,0,1,1)
		
	def p90_hard_pulse(self):
	
		# Excitation Pulse
		amp = self.power_auc*self.samp_rate 
		foo = pulse_shape.hard_pulse(self.p90,self.samp_rate)
		data = foo/sum(foo)*amp						# define pulse data

		# check if aplitude is greater than 1
		if ((np.max(np.real(data))>1) or (np.max(np.imag(data))>1)):
			warnings.simplefilter('error', UserWarning)
			warnings.warn('EXCITATION PULSE AMPLITUDE GREATER THAN 1V')

		tb.ex_pulse.set_data(data)					# update pulse in flowgraph
		start = int(self.fudge*self.samp_rate+params.gr_ramp);	  # define pulse start time
		pulses.ex = pulse(data,start)				# create pulse object
		tb.set_ex_delay(start) 						# update pulse delay
		if (self.ischopped == 1):
			tb.ex_pulse.set_amps(1,-2,2,self.nav)
		else:
			tb.ex_pulse.set_amps(1,0,1,1)
	
	def p180_hard_pulse(self):
		# Refocusing Pulse
		amp = 2*self.power_auc*self.samp_rate			# set area to twice 90 degree pulse area
		data = pulse_shape.hard_pulse(self.p180,self.samp_rate)
		data = data/sum(data)*amp;

		# check if aplitude is greater than 1
		if ((np.max(np.real(data))>1) or (np.max(np.imag(data))>1)):
			warnings.simplefilter('error', UserWarning)
			warnings.warn('REFOCUSING PULSE AMPLITUDE GREATER THAN 1V')

		tb.ref_pulse.set_data(data)				 # update pulse in flowgraph
		start = int(self.samp_rate*(self.TE/2-self.p180/2+self.p90/2)+params.gr_ramp) # set delay
		tb.set_ref_delay(start) 					# update delay in flowgraph
		pulses.ref = pulse(data,start)				# create pulse object


	
	def readout_pulse(self):	
		# Readout Window
		self.readout_length = round(float(self.samp_rate)/self.BW*self.SI)	# define pulse length
		self.readout_time = self.readout_length/self.samp_rate				# define pulse length (time)
		data = array([1.]*int(self.samp_rate*self.readout_time))	# define data
		tb.readwin.set_data(data)  											# update pulse in flowg graph
		start = int(self.samp_rate*(self.TE+self.p90/2)+self.gr_ramp-data.size/2) #define start time
		tb.set_readout_delay(start)	 									# update pulse delay
		pulses.readout = pulse(data,start)									# create pulse object
		
	def set_readgrad(self):
		## Readout Gradient
		
		# define gradient amplitude from gradient strength data
		self.read_amp = self.read_fudge*self.BW/(self.FOVread*4258*self.Grad_str[self.readdir])
		# define prephasor data portion
		g2 = pulse_shape.grad_trap(self.readout_time,self.gr_ramp,self.samp_rate)*self.read_amp
		
		# readout data
		g1 = pulse_shape.grad_trap(self.pre_dur,self.gr_ramp,self.samp_rate)
		g1 = self.prephasor_fudge*-g1*sum(g2)/sum(g1)/2
		
		
		start = int(self.samp_rate*(self.TE+self.p90/2)-g2.size/2-g1.size+self.gr_ramp)				# define prephasor start time
		
		data = concatenate((g1,g2))
		pulses.readgrad = pulse(data,start)					# create pulse object
		
		if (np.amax(np.abs(self.read_amp)>1)):
			warnings.simplefilter('error', UserWarning)
			warnings.warn('READOUT GRADIENT AMPLITUDE GREATER THAN 1V')
			
						  
		# Set data and delays based on desired gradient directions
		if (self.readdir == x):
			# X Gradient
			self.xgrad = data
			tb.Gx.set_data(self.xgrad)
			tb.Gx.set_amps(1,0,1,1)
			tb.set_Gx_delay(start)
		elif (self.readdir == y):
			# Y Gradient
			self.ygrad = data
			tb.Gy.set_data(self.ygrad)
			tb.Gy.set_amps(1,0,1,1)
			tb.set_Gy_delay(start)
		elif (self.readdir == z):
			# Z Gradient
			self.zgrad = data
			tb.Gz.set_data(self.zgrad)
			tb.Gz.set_amps(1,0,1,1)
			tb.set_Gz_delay(start)

		
	def set_phasegrad(self):
		## Phase encode gradient
		data = pulse_shape.grad_trap(self.phase_dur,5,self.samp_rate)	# set pulse data
		
		# calculate phase step from gradient calibration file
		self.phase_step = 1./(4258*self.Grad_str[self.phasedir]*(sum(data)/params.samp_rate)*self.FOVphase)
		# calculate starting amplitude
		self.phase_amp = -self.phase_step*self.NPE/2
				
		start = int(self.samp_rate*self.p90+2*self.gr_ramp)		# define pulse start
		
		pulses.phasegrad = pulse(data,start)					# create pulse object
		
		# check if gradient aplitude is greater than 1
		if (np.abs(self.phase_amp)>1):
			warnings.simplefilter('error', UserWarning)
			warnings.warn('PHASE ENCODE GRADIENT AMPLITUDE GREATER THAN 1V')
			
		# set data, delays, and phase steps for desired gradient directions
		if (self.phasedir == x):
			# X Gradient
			self.xgrad = data
			tb.Gx.set_data(self.xgrad)
			tb.Gx.set_amps(self.phase_amp,self.phase_step,self.NPE,self.nav)
			tb.set_Gx_delay(start)
		elif (self.phasedir == y):
			# Y Gradient
			self.ygrad = data
			tb.Gy.set_data(self.ygrad)
			tb.Gy.set_amps(self.phase_amp,self.phase_step,self.NPE,self.nav)
			tb.set_Gy_delay(start)
		elif (self.phasedir == z):
			# Z Gradient
			self.zgrad = data
			tb.Gz.set_data(self.zgrad)
			tb.Gz.set_amps(self.phase_amp,self.phase_step,self.NPE,self.nav)
			tb.set_Gz_delay(start)
			
	
	def set_slicegrad(self):
		# Slice Gradient
		if (self.ex_type == 0):		# Check excitation type
			self.sliceamp = 0
		else:
			# calculate amplitude from gradient calibration file info
			self.sliceamp = self.TBW/(self.p90*4258*self.Grad_str[self.slicedir]*self.slice_thick)
		
		g1 = pulse_shape.grad_trap(self.p90,self.gr_ramp,self.samp_rate)*self.sliceamp
		g2 = pulse_shape.grad_trap(self.rewinder_length,self.gr_ramp,self.samp_rate)
		g2 = self.rephase_fudge*g2*(sum(g1[25:-25])/sum(g2))
		
		dead = int(self.samp_rate*(-self.p90/2+self.TE/2+self.p180/2));
		dead = array([0.]*dead)
		data = concatenate((g1,dead,g2))
		
		start = 0					# define gradient start
		
		pulses.slicegrad = pulse(data,start)						# create pulse object
		
		# Check to make sure slice select gradient is less than 1v
		if (np.abs(self.sliceamp)>1):
			warnings.simplefilter('error', UserWarning)
			warnings.warn('SLICE GRADIENT AMPLITUDE GREATER THAN 1V')
		# check to make sure rephase gradient less than 1v
		if (max(np.abs(data))>1):
			warnings.simplefilter('error', UserWarning)
			warnings.warn('SLICE REWINDER GRADIENT AMPLITUDE GREATER THAN 1V')
				
		# set data, delays, and phase steps for desired gradient directions
		if (self.slicedir == x):
			# X Gradient
			self.xgrad = data
			tb.Gx.set_data(self.xgrad)
			tb.Gx.set_amps(1,0,1,1)
			tb.set_Gx_delay(start)
		elif (self.slicedir == y):
			# Y Gradient
			self.ygrad = data
			tb.Gy.set_data(self.ygrad)
			tb.Gy.set_amps(1,0,1,1)
			tb.set_Gy_delay(start)
		elif (self.slicedir == z):
			# Z Gradient
			self.zgrad = data
			tb.Gz.set_data(self.zgrad)
			tb.Gz.set_amps(1,0,1,1)
			tb.set_Gz_delay(start)
	
# Interactive commands
	
	def param_table(self):
	# Display table with parameters
		dtype = [('param','|S25'),('value',float),('units','|S25')]
		tab = np.array([],dtype=dtype)
		f = open('spinecho_config.txt','r')
		
		for line in f:
			k,v,bar = [q.strip() for q in line.split(',')]
			if k == 'param_file':
				pass
			elif k == 'interactive_mode':
				pass
			elif k == 'leader_ID':
				pass
			elif k == 'follower_ID':
				pass
			else:
			
				s = 'params.' + k
				b = eval(s)
				
				values = [(k,float(v),bar.lstrip())]
				foo = np.asarray(values,dtype=dtype)
				tab = np.append(tab,foo)
			
		param_table = tabulate(tab,headers=["Param Name","Current Value","Units"])
		print param_table
	
	def set_TR(self,TR):
		# update everything dependent on TR
		self.TR = float(TR)
		tb.set_TR(TR)
		print "Set TR to: " + repr(self.TR)
			
	def set_TE(self,TE):
		if TE>self.TR: print("Warning: TE too large") # make sure delay is reasonable
		else: 
			try:
				# update everything dependent on TE
				self.TE = TE
				if (self.ex_type==0):
					self.p90_hard_pulse()
				elif (self.ex_type==1):
					self.p90_sinc_pulse()
				self.p180_hard_pulse()
				self.set_readgrad()
				self.readout_pulse()
				self.set_slicegrad()
				print "Set TE to: " + repr(self.TE)
			# make sure gradients aren't overlapping
			except: print "GRADIENT OVERLAP: TE TOO SHORT, TE NOT SET"
	
	def set_p90(self,p90):
		# update everything dependent on p90
		self.p90 = p90
		if (self.ex_type==0):
			self.p90_hard_pulse()
		elif (self.ex_type==1):
			self.p90_sinc_pulse()
		self.p180_hard_pulse()
		self.readout_pulse()
		self.set_readgrad()
		self.set_phasegrad()
		self.set_slicegrad()
		#tb.set_power(self.power)
		print "Set p90 to: " +repr(self.p90)
	
	def set_ischopped(self,ischopped):
		# update everything dependent on ischopped
		self.ischopped = ischopped
		if (self.ex_type==0):
			self.p90_hard_pulse()
		elif (self.ex_type==1):
			self.p90_sinc_pulse()
		print "Set ischopped to: " + repr(self.ischopped)
	
	def set_slice_shift(self,sliceshift):
	# update everything dependent on sliceshift
		self.slice_shift = sliceshift
		if (self.ex_type==0):
			self.p90_hard_pulse()
		elif (self.ex_type==1):
			self.p90_sinc_pulse()
		print "Set slice_shift to: " + repr(self.slice_shift)

		
	def set_SI(self,SI):
		try:
			# update everything dependent on TE
			self.SI = SI
			self.set_readgrad()
			self.readout_pulse()
			print "Set SI to: " + repr(self.SI) + " and adjusted readout gradient"
		# make sure gradients aren't overlapping
		except: print "GRADIENT OVERLAP: TE TOO SHORT, SI NOT SET"
	
	def set_BW(self,BW):
	# update everything dependent on BW
		self.BW = BW
		self.readout_length = round(float(self.samp_rate)/self.BW*self.SI)
		self.readout_time = self.readout_length/self.samp_rate
		
		self.BW = self.samp_rate*float(self.SI)/self.readout_length
		self.set_readgrad()
		self.readout_pulse()
		self.set_slicegrad()
		self.set_phasegrad()
		tb.qtgui_time_sink_x_0.set_nsamps(int(self.readout_length))
		print "Exact Bandwidth: " + repr(self.BW)
			
	
	def set_FOVread(self,FOVread):
		# update everything dependent on FOVread
		self.FOVread = FOVread
		self.set_readgrad()
		print "FOVread set to: " + repr (FOVread)
		
	def set_FOVphase(self,FOVphase):
		# update everything dependent on FOVphase
		self.FOVphase = FOVphase
		self.set_phasegrad()
		print "FOVphase set to: " + repr (FOVphase)
		
	def set_pre_dur(self,pre_dur):
		# update everything dependent on pre_dur
		self.pre_dur = pre_dur
		self.set_readgrad()
		print "pre_dur set to: " + repr(pre_dur)
		
	def set_phase_dur(self,phase_dur):
		# update everything dependent on phase_dur
		self.phase_dur = phase_dur
		self.set_phasegrad()
		print "phase_dur set to: " + repr(phase_dur)
		
	def set_rewinder_length(self,rewinder_length):
		# update everything dependent on rewinder_length
		self.rewinder_length = rewinder_length
		self.set_slicegrad()
		print "rewinder_length set to: " + repr(rewinder_length)

	
	def set_slice_thick(self,slice_thick):
		# update everything dependent on slice_thick
		self.slice_thick = slice_thick
		self.set_slicegrad()
		print "slice_thick set to: " + repr(slice_thick)
		
	def set_NPE(self,NPE):
		# update everything dependent on NPE (phase encodes)
		self.NPE = NPE
		self.phase_step = -self.phase_amp*2/self.NPE
		self.set_phasegrad()
		print "Set NPE to: " + repr(self.NPE)
	
	def set_nav(self,nav):
		# update everything dependent on nav
		self.nav = nav
		self.set_phasegrad()
		if (self.ex_type==0):
			self.p90_hard_pulse()
		elif (self.ex_type==1):
			self.p90_sinc_pulse()
		print "Set nav to: " + repr(self.nav)
		
	def set_TBW(self,TBW):
		# update everything dependent on TBW
		self.TBW = TBW
		if (self.ex_type==0):
			self.p90_hard_pulse()
		elif (self.ex_type==1):
			self.p90_sinc_pulse()
		self.set_slicegrad()
		print "Set TBW to: " + repr(self.TBW)
		
	def set_ex_type(self,ex_type):
		# update everything dependent on ex_type
		# 0 for hard pulse, 1 for windowed sinc
		self.ex_type = ex_type
		if (self.ex_type==0):
			self.p90_hard_pulse()
		elif (self.ex_type==1):
			self.p90_sinc_pulse()
		self.set_slicegrad()
		print "Set ex_type to: " + repr(ex_type)
	   
			
	def save_params(self,filename):
		# save current parameters to pickle file
		import os.path
		out = vars(self)	   			# create dictionary from parameters structure
		f = filename + '.pkl'   		# define file path
		
		# check for current file and ask overwrite permission
		if os.path.isfile(f):
			x = raw_input("A file named " + repr(f) + " already exists.  Overwrite? (Y/N)  ")
			if x == 'Y':
				output = open(f,'wb')   # open file
				pickle.dump(out,output) # save dictionary to file
				output.close()		  # close file
			elif x == 'N':
				pass
			else:
				print "Invalid Entry\n"
		else:
			output = open(f,'wb')	   # open file
			pickle.dump(out,output)	 # save dictionary to file
			output.close()			  # close file
		
	def import_params(self,filename):
		try:
			p = open(filename, 'rb')	# open file
			info = pickle.load(p)	   # load dictionary
			p.close()				   # close file
			self.__dict__.update(info)  # update parameter struct with dictionary
			# reset pulses with new parameters
			if (self.ex_type==0):
				self.p90_hard_pulse()
			elif (self.ex_type==1):
				self.p90_sinc_pulse()
			self.p180_hard_pulse()
			self.readout_pulse()
			self.set_slicegrad()
			self.set_readgrad()
			self.set_phasegrad()
			self.set_TR(self.TR)
		except:
			print "Invalid Filename"   # return error if filename does not exist

				  
def read_on():
	# enable readout gradient on appropriate channel
	if params.readdir == x:
		tb.set_gx_on(1)
	elif params.readdir == y:
		tb.set_gy_on(1)
	elif params.readdir == z:
		tb.set_gz_on(1)
	print "Turned on Readout Gradient"
		
def slice_on():
	# enable slice gradient on appropriate channel
	if params.slicedir == x:
		tb.set_gx_on(1)
	elif params.slicedir == y:
		tb.set_gy_on(1)
	elif params.slicedir == z:
		tb.set_gz_on(1)
	print "Turned on Slice Gradient"

def grads_off():
	# disable output of all gradients
	tb.set_gx_on(0)
	tb.set_gy_on(0)
	tb.set_gz_on(0)
	print "Turned off Gradients"  

def profile():
	# Display 1D profile along readout
	tb.set_RUN(0)								# pause sequence
	tb.signal_out.reset()						# clear signal_out data
	tb.ex_pulse.restart()						# reset excitation pulse
	# initialize profile data
	params.profdata = zeros([1,params.readout_length],complex)		# allocate profdata with zeros
	scan_time = params.nav*params.TR			# define scan time
	
	def read():
		tb.set_RUN(0)
		# save raw data from flowgraph
		rawdata = np.asarray(tb.signal_out.data())[-int(params.readout_length*params.nav):]
		# format and average raw data
		params.profdata = np.sum(np.reshape(rawdata,[params.nav,np.size(rawdata)/params.nav]),0)/params.nav
		
		# interpolate and decimate data to appropriate size
		if params.readout_length%params.SI!=0:
			foo = ceil(params.readout_length/params.SI)*params.SI
			x = np.arange(0,params.readout_length)
			f = interpolate.interp1d(x,params.profdata)
			xnew = np.arange(0,params.readout_length-1,(params.readout_length-1)/foo)
			params.profdata = f(xnew)
		
		params.profdata = sig.decimate(params.profdata,int(params.profdata.size/params.SI),60,'fir')		   
		print "****SCAN COMPLETE****"
		tb.set_RUN(1)
	
	tb.set_RUN(1)									# resume scan
	proftimer = threading.Timer(scan_time,read,[])	# set timer for reading data
	proftimer.start()								# start timer thread
	proftimer.join()								# wait for thread to catch back up
	
	
	# ifft recon of center slice
	prof = np.abs(np.fft.fftshift(np.fft.ifft(np.fft.fftshift(params.profdata))))
	
	# plot results
	plt.clf()
	plt.title('Quick Profile')
	plt.plot(prof);plt.show();plt.draw()
	return prof
	tb.set_RUN(1)  

def enforce_fov():
	# adjust readout gradient to requested FOV - requires knowledge of object size
	w = float(input("Input object size in readout dimension (mm): "))
	
	P = profile()
	
	objpix = P>(.2*amax(P))
	d1 = np.argmax(objpix)
	d2 = np.argmax(np.flipud(objpix))-1
	noise = append(P[:round(d1*.75)],P[round(-d2*.75):])
	objpix = P>(amax(noise)*1.1)
	d1 = np.argmax(objpix)
	d2 = P.size-np.argmax(np.flipud(objpix))-1
	
	pix = w/params.FOVread*params.SI
	
	params.read_fudge = params.read_fudge*(pix/(d2-d1))
	
	params.set_readgrad()  

def run():
	# time stamp
	tb.sync_data.reset()
	ts = time.time()
	data.rawdata = [0.+0.j]*int(params.readout_length)
	data.time_stamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
	
	def check_sync():
		# Check to make sure radios are still synchronized
		sig = tb.sync_data.data()[-20:]
		a = max(real(sig))-max(imag(sig))
		if a>2:
			params.underflow += 1
			sync()
		#tb.sync_data.reset()
		
	def end_scan():
		tb.set_RUN(0)	# pause scan
		grads_off()		# turn off gradients
		data.rawdata = data.rawdata[params.readout_length:]
		#data.rawdata = np.asarray(tb.signal_out.data()[:int(scan_size)]) # save data to rawdata
		print "****SCAN COMPLETE****"
		tb.set_RUN(1)	# resume scan
	
	scan_time = params.TR*params.nav*(params.NPE)							# define scan time
	scan_size = params.samp_rate*params.nav*params.NPE*params.readout_time	# define scan data size
	
	def scan_monitor():
	# function to monitor synchronization during scan
		l = 0
		while l <= params.NPE*params.nav: 	 	# exit when scan is complete
			l+=1
			check_sync()										# defined above
			data.rawdata = concatenate([data.rawdata,tb.signal_out.data()[-int(params.readout_length):]],axis=1)
			progress = float(l)/(params.NPE*params.nav)	# calculate how much of scan complete
			t_left = (1-progress)*scan_time
			prog_pct = progress*100
			minutes = t_left / 60
			seconds = t_left % 60
			print "Progress: %2d %%,\t\tTime: %d:%2d" % (prog_pct,minutes,seconds) # display countdown
			time.sleep(float(params.TR))	# subtracted .05 because computer timer is a little bit slow
		
		end_scan()	# defined above
	
	t = threading.Thread(name='scanner',target=scan_monitor,args=())	# set up thread for scan monitor
	t.daemon = True
	
	# turn on and restart gradients
	tb.set_RUN(0)
	time.sleep(1)
	tb.set_gx_on(1)
	tb.set_gy_on(1)
	tb.set_gz_on(1)
	tb.Gx.restart()
	tb.Gy.restart()
	tb.Gz.restart()
	tb.ex_pulse.restart()
	tb.signal_out.reset()
	tb.sync_data.reset()

	time.sleep(params.TR/2)		# wait for a half tr
	t.start()					# start monitor
	time.sleep(params.TR/3)
	tb.set_RUN(1)				# start scan
		
	
def end():
	# closes flowgraph program
	tb.stop()
	tb.close()
	thread.interrupt_main()
		
def show_pulses():
	# displays pulses in a figure
	T = int(params.samp_rate*(params.TE+params.readout_time/2)+2*params.gr_ramp)
	length = T+10
	step = 1./params.samp_rate
	
	names = [];
	
	for name in dir(pulses):
		if not(name.startswith('__')):
			start = getattr(pulses,name).start
			data = real(getattr(pulses,name).data)
			if name=='phasegrad':
				dead1 = [0.]*start
				dead2 = length-start-data.size
				dead2 = [0.]*dead2
				foo = concatenate((dead1,data*params.phase_amp,dead2))
			else:
				dead1 = [0.]*start
				dead2 = length-start-data.size
				dead2 = [0.]*dead2
				foo = concatenate((dead1,data,dead2))
			t = np.asarray(range(foo.size))*step*1000 # in milliseconds
			plt.plot(t,foo);
			names.append(name);

	plt.xlabel("Time (ms)")
	plt.ylabel("Amplitude (V)")
	plt.title("Pulse Sequence")
	legend(names)
	draw()


def sync():
	print "syncing..."
	tb.set_RUN(0)										   # pause scan
	
	sig = tb.sync_data.data()[-20:]						 # grab data from last TR
	s1 = max(real(sig))									 # leader delay
	s2 = max(imag(sig))									 # follower delay
	d = s1-s2											   # difference
	
	if d>0: tb.set_leader_delay(tb.leader_delay+abs(d));	# set correct delay
	if d<0: tb.set_follower_delay(tb.follower_delay+abs(d));# set correct delay
	
	tb.set_RUN(1)										   # resume scan

def calib_slice():
	# finds fudge factor to recover all phase from slice encode
	step = .025
	slice_on()					# turn on slice grad
	tb.signal_out.reset()		# reset signal vector
	oldTR = params.TR			# save old TR
	params.set_TR(1)			# set TR to 1 for this search
	time.sleep(1)
	dat = np.asarray([]);
	tb.set_RUN(0)
	params.rephase_fudge = .4	# start search at .4
	x = ()
	count = 0
	while True:
		count+=1
		params.rephase_fudge += step	# step fudge factor forward
		params.set_slicegrad()			# recreate slice gradient
		tb.set_RUN(1)					# run acquisition
		time.sleep(params.TR)
		tb.set_RUN(0)					# pause scan
		foo = np.asarray(np.abs(tb.signal_out.data()[-int(params.readout_length):]))  # save data
		dat = np.append(dat,np.sum(foo))	# save sum of data to dat
		
		plt.clf()
		# display data
		x = np.append(x,params.rephase_fudge)	
		plt.plot(x,dat)
		plt.xlabel("Rewinder Scalar")
		plt.ylabel("Signal Amplitude (A.U.)")
		plt.show()
		plt.draw()
		
		if count>4:	# wait for a few runs
			if (0.6*np.amax(dat[:-3]))>dat[-1]:
				break		# break if data is less than .4 times the max value of dat
			elif params.rephase_fudge>2:
				break
	
	params.rephase_fudge = x[np.argmax(dat)]
	params.set_slicegrad()
	print "Max power at " +repr(x[np.argmax(dat)])
	
	tb.set_RUN(1)
	params.set_TR(oldTR)
	
def calib_readout():
	read_on()
	p1 = params.prephasor_fudge		   	# current prephasor amplitude
		
	sig1 = np.abs(np.asarray(tb.signal_out.data()[int(-params.readout_length):]))
	c1 = float(np.argmax(sig1))				# current signal center
	
	params.prephasor_fudge = p2 = p1+.2	   # add small amount to prephasor amp
	params.set_readgrad()
	
	time.sleep(1.5*params.TR)	 			# wait for data
	sig2 = np.abs(np.asarray(tb.signal_out.data()[int(-params.readout_length):]))
	c2 = float(np.argmax(sig2))				# find new signal center
	
	slope = (c2-c1)/(p2-p1)					# calculate slope of power/center ind
	# set prephasor power necessary to center signal
	params.prephasor_fudge = newamp = (params.readout_length/2.-c2)/slope+p2
	# update new gradient
	params.set_readgrad()
	print "Prephasor amplitude set to " + repr(np.round(newamp,decimals=2))
					
		
if __name__ == '__main__':
	data = dat()				# set data structure
	params = pulse_params()			 # set scan parameters structure
	
	try:
		params.import_params(params.filename)
	except:
		print "No file named %s" % params.filename
		
	tb.set_offset(params.offset)
	# initialize and set pulses
	tb.set_TR(params.TR)
	tb.qtgui_time_sink_x_0.set_nsamps(int(params.readout_length))
	
	if (params.ex_type==0):
		params.p90_hard_pulse()
	elif (params.ex_type==1):
		params.p90_sinc_pulse()
		
	params.p180_hard_pulse()
	params.readout_pulse()
		   
	# initialize and set gradients
	params.set_readgrad()
	params.set_phasegrad()
	params.set_slicegrad()
	
	time.sleep(1)
	sync()	 # synchronize radios
	
	if (interactive_mode==0):
		run()
